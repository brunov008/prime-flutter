import 'package:flutter/material.dart';
import 'package:prime/blocs/login.bloc.dart';
import 'package:prime/models/user.model.dart';
import 'package:prime/ui/widget.utils.dart';
import 'package:prime/ui/widgets/progress_indicator.ui.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatelessWidget {

  static const routeName = '/login';

  final _scalfoldkey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  String _email;
  String _senha;

  @override
  Widget build(BuildContext context) {
    final LoginBloc bloc = Provider.of<LoginBloc>(context);

    return Scaffold(
      key: _scalfoldkey,
      body:
      bloc.isLoading ?
      Center(child: GenericProgressIndicator()):
      SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child:
        Container(
          child: Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height/2.5,
                decoration: BoxDecoration(
                  /*
                    image: DecorationImage(
                        image: AssetImage(''), fit: BoxFit.cover),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(90)
                    )
                   */
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Spacer(),
                    Align(
                        alignment: Alignment.center,
                        child: Padding(
                          padding: EdgeInsets.only(top: 10.0),
                          child: SizedBox(
                            height: 150.0,
                            /*
                            child: Image.asset(
                              "",
                              fit: BoxFit.contain,
                            ),
                             */
                          ),
                        )
                    ),
                    Spacer(),
                  ],
                ),
              ),

              Form(
                key: _formKey,
                child: Container(
                  height: MediaQuery.of(context).size.height/2,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(top: 62),
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width/1.2,
                        height: 45,
                        padding: EdgeInsets.only(
                            top: 4,left: 16, right: 16, bottom: 4
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(50)
                            ),
                            color: Theme.of(context).accentColor,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black12,
                                  blurRadius: 5
                              )
                            ]
                        ),
                        child: TextFormField(
                          autofocus: false,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value){
                            /*
                            if(value.isEmpty && !value.contains('@')){
                              return 'E-mail inválido';
                            }
                             */
                            return null;
                          },
                          onSaved: (input) => _email = input,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(Icons.email,
                              color: Colors.grey,
                            ),
                            hintText: 'Email',
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width/1.2,
                        height: 45,
                        margin: EdgeInsets.only(top: 32),
                        padding: EdgeInsets.only(
                            top: 4,left: 16, right: 16, bottom: 4
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(50)
                            ),
                            color: Theme.of(context).accentColor,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black12,
                                  blurRadius: 5
                              )
                            ]
                        ),
                        child: TextFormField(
                          autofocus: false,
                          keyboardType: TextInputType.text,
                          validator: (value){
                            /*
                            if(value.isEmpty && value.length < 6){
                              return 'Senha inválida';
                            }
                             */
                            return null;
                          },
                          onSaved: (input) => _senha = input,
                          obscureText: true,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(Icons.vpn_key,
                              color: Colors.grey,
                            ),
                            hintText: 'Senha',
                          ),
                        ),
                      ),
                      /*
                      FlatButton(
                        onPressed: () => Navigator.pushNamed(context, '/recover'),
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 16, right: 32
                            ),
                            child: Text('Esqueceu a senha?',
                              style: TextStyle(
                                  color: Colors.grey
                              ),
                            ),
                          ),
                        ),
                      ),
                       */
                      Spacer(),
                      TextButton(
                        onPressed: (){
                          if(_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            var user = new User(email: _email.trim(), password: _senha.trim());

                            bloc.doLogin(user, () =>  Navigator.popAndPushNamed(context, '/home'),
                                    (message) => WidgetsUtils.customSnack(context, message, false)
                            );
                          }
                        },
                        child: Container(
                          height: 45,
                          width: MediaQuery.of(context).size.width/1.2,
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.all(
                                  Radius.circular(50)
                              )
                          ),
                          child: Center(
                            child: Text('Entrar'.toUpperCase(),
                              style: TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      )
    );
  }
}

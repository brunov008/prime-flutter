import 'package:flutter/material.dart';
import 'package:prime/blocs/home.bloc.dart';
import 'package:prime/pages/home/close.feed.page.dart';
import 'package:prime/pages/home/feed.page.dart';
import 'package:prime/pages/home/find.feed.page.dart';
import 'package:prime/ui/widgets/drawer.ui.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {

  static const routeName = '/home';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final _scalffoldkey = GlobalKey<ScaffoldState>();
  int _currentIndex = 0;
  bool shouldShowActionButton = false;

  final List<Widget> _children = [
    FeedPage(),
    CloseFeedPage(),
    FindFeedPage()
  ];

  @override
  Widget build(BuildContext context) {
    final HomeBloc bloc = Provider.of<HomeBloc>(context);

    if(!bloc.alreadyLoad) bloc.getFeedList();

    return DefaultTabController(
      length: _children.length,
      child: Scaffold(
        key: _scalffoldkey,
        drawer: CustomDrawer(),
        bottomNavigationBar: BottomNavigationBar(
          onTap: onTabTapped,
          currentIndex: _currentIndex,
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label: ""
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.list),
                label: ""
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.search),
                label: ""
            )
          ],
        ),
        body: _children[_currentIndex],
      ),
    );
  }

  void onTabTapped(int index){
    setState(() => _currentIndex = index);
  }
}


import 'dart:async';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:prime/blocs/home.bloc.dart';
import 'package:prime/blocs/login.bloc.dart';
import 'package:prime/pages/login/login.page.dart';
import 'package:prime/routes.dart';
import 'package:prime/themes/light.theme.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart' show kDebugMode;

const _kTestingCrashlytics = true;

void main(){
  WidgetsFlutterBinding.ensureInitialized();
  runZonedGuarded(() {
    runApp(CustomProvider());
  }, (error, stackTrace) {
    FirebaseCrashlytics.instance.recordError(error, stackTrace);
  });
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  Future<void> _initializeFlutterFire() async {
    // Wait for Firebase to initialize
    await Firebase.initializeApp();

    if (_kTestingCrashlytics) {
      // Force enable crashlytics collection enabled if we're testing it.
      await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
    } else {
      // Else only enable it in non-debug builds.
      // You could additionally extend this to allow users to opt-in.
      await FirebaseCrashlytics.instance
          .setCrashlyticsCollectionEnabled(!kDebugMode);
    }

    // Pass all uncaught errors to Crashlytics.
    Function originalOnError = FlutterError.onError;
    FlutterError.onError = (FlutterErrorDetails errorDetails) async {
      await FirebaseCrashlytics.instance.recordFlutterError(errorDetails);
      // Forward to original handler.
      originalOnError(errorDetails);
    };
  }

  @override
  void initState() {
    super.initState();
    //_initializeFlutterFire();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Prime',
      theme: lightTheme(),
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
      routes: routes,
    );
  }
}

class CustomProvider extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<LoginBloc>.value(
              value : LoginBloc()
          ),
          ChangeNotifierProvider<HomeBloc>.value(
              value: HomeBloc()
          ),
        ],
        child: MyApp()
    );
  }
}
import 'package:flutter/material.dart';

const brightness = Brightness.light;

const primeblue = Colors.blue; // Azul Claro

const success = const Color.fromARGB(255, 16, 172, 132); // Verde de success
const successLight = const Color.fromARGB(255, 29, 209, 161); // Segundo verde de success

// abaixo cores que definem se o tema fica dark ou light
const nocturn = const Color.fromARGB(255, 34, 36, 40);  // Cinza bem escuro
const albine = const Color.fromARGB(255, 255, 255, 255); // Branco

ThemeData lightTheme() {
  return ThemeData(
      brightness: brightness,

      primaryColor: primeblue,
      backgroundColor: primeblue,

      primaryColorDark: brightness != Brightness.dark
          ? albine : nocturn,
      accentColor: brightness == Brightness.dark
          ? nocturn : albine,
      textTheme: new TextTheme(
          bodyText2: new TextStyle(color: brightness != Brightness.dark ? Colors.black54 : Colors.white),
          headline4: new TextStyle(
            color: brightness == Brightness.dark
                ? albine : nocturn,
          ),
          button: new TextStyle(color: success)
      )
  );
}
class FeedModel {
  List<Result> result;

  FeedModel({this.result});

  FeedModel.fromJson(Map<String, dynamic> json) {
    if (json['result'] != null) {
      result = new List<Result>();
      json['result'].forEach((v) {
        result.add(new Result.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Result {
  int id;
  Autor autor;
  String message;
  List<Assets> assets;
  List<Comments> comments;

  Result({this.id, this.autor, this.message, this.assets, this.comments});

  Result.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    autor = json['autor'] != null ? new Autor.fromJson(json['autor']) : null;
    message = json['message'];
    if (json['assets'] != null) {
      assets = new List<Assets>();
      json['assets'].forEach((v) {
        assets.add(new Assets.fromJson(v));
      });
    }
    if (json['comments'] != null) {
      comments = new List<Comments>();
      json['comments'].forEach((v) {
        comments.add(new Comments.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.autor != null) {
      data['autor'] = this.autor.toJson();
    }
    data['message'] = this.message;
    if (this.assets != null) {
      data['assets'] = this.assets.map((v) => v.toJson()).toList();
    }
    if (this.comments != null) {
      data['comments'] = this.comments.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Autor {
  int id;
  String photo;
  String username;

  Autor({this.id, this.photo, this.username});

  Autor.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    photo = json['photo'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['photo'] = this.photo;
    data['username'] = this.username;
    return data;
  }
}

class Assets {
  String path;
  String subtitle;
  Localization localization;
  List<UsernamesIdentify> usernamesIdentify;

  Assets({this.path, this.subtitle, this.localization, this.usernamesIdentify});

  Assets.fromJson(Map<String, dynamic> json) {
    path = json['path'];
    subtitle = json['subtitle'];
    localization = json['localization'] != null
        ? new Localization.fromJson(json['localization'])
        : null;
    if (json['usernamesIdentify'] != null) {
      usernamesIdentify = new List<UsernamesIdentify>();
      json['usernamesIdentify'].forEach((v) {
        usernamesIdentify.add(new UsernamesIdentify.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['path'] = this.path;
    data['subtitle'] = this.subtitle;
    if (this.localization != null) {
      data['localization'] = this.localization.toJson();
    }
    if (this.usernamesIdentify != null) {
      data['usernamesIdentify'] =
          this.usernamesIdentify.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Localization {
  double long;
  double lat;

  Localization({this.long, this.lat});

  Localization.fromJson(Map<String, dynamic> json) {
    long = json['long'];
    lat = json['lat'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['long'] = this.long;
    data['lat'] = this.lat;
    return data;
  }
}

class UsernamesIdentify {
  int id;
  String username;

  UsernamesIdentify({this.id, this.username});

  UsernamesIdentify.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    return data;
  }
}

class Comments {
  User user;
  String comment;

  Comments({this.user, this.comment});

  Comments.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    comment = json['comment'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['comment'] = this.comment;
    return data;
  }
}

class User {
  int id;
  String photo;
  String username;
  String name;
  String surname;

  User({this.id, this.photo, this.username, this.name, this.surname});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    photo = json['photo'];
    username = json['username'];
    name = json['name'];
    surname = json['surname'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['photo'] = this.photo;
    data['username'] = this.username;
    data['name'] = this.name;
    data['surname'] = this.surname;
    return data;
  }
}
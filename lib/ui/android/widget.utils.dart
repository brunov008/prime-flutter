import 'package:flutter/material.dart';

class WidgetsUtils{

  static customSnack(context, message, isInfinity){
    return SnackBar(
        content: Text(
          message,
        ),
        backgroundColor: Theme.of(context).primaryColor,
        duration: (isInfinity)? Duration(days: 365) : Duration(seconds: 2)
    );
  }
}
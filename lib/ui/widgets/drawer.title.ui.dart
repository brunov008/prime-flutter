import 'package:flutter/material.dart';

class DrawerTitle extends StatelessWidget {

  final IconData icon;
  final String text;
  final GestureTapCallback click;

  DrawerTitle(this.icon, this.text, this.click);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: click,
        child: Container(
          height: 60.0,
          padding: EdgeInsets.only(left: 20.0),
          child: Row(
            children: <Widget>[
              Icon(
                icon,
                size: 25.0,
                color: Colors.black,
              ),
              SizedBox(
                width: 32.0,
              ),
              Text(
                text,
                style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.black,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

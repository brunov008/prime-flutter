import 'package:flutter/material.dart';

class CircularImage extends StatelessWidget {

  String image;
  double horizontal;
  double vertical;

  CircularImage(this.image, {this.horizontal, this.vertical});

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: FractionalOffset.centerLeft,
        padding: EdgeInsets.symmetric(
            horizontal: horizontal != null ? horizontal : 16.0,
            vertical: vertical != null ? vertical : 12.0
        ),
        child: Row( //TODO Transformar em lista
          children: [
            image != null ?
            ClipRRect(
              borderRadius: BorderRadius.circular(50.0),
              child: Image(
                  height: 55,
                  width: 55,
                  image: AssetImage(image)
              ),
            ) : Container()
          ],
        )
    );
  }
}

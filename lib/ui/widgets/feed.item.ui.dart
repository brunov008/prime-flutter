import 'package:flutter/material.dart';
import 'package:prime/blocs/home.bloc.dart';
import 'package:prime/ui/widgets/circular.image.ui.dart';
import 'package:prime/ui/widgets/single.image.ui.dart';
import 'package:prime/ui/widgets/swipe.ui.dart';
import 'package:provider/provider.dart';

import '../../pages/home/messages/message.item.page.dart';

class FeedItem extends StatelessWidget {

  int index;

  FeedItem(this.index);

  @override
  Widget build(BuildContext context) {
    final HomeBloc bloc = Provider.of<HomeBloc>(context);

    return Container(
      margin: EdgeInsets.only(top: 31),
      height: 390.0,
      child: Stack(
        children: [
          bloc.list[index].assets.length > 1 ? MultipleAssetsSwipe(index) : SingleAsset(index),
          Positioned.fill(
              bottom: 30,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: FractionallySizedBox(
                  widthFactor: 0.88,
                  child: Container(
                    padding: EdgeInsets.only(top: 15, left: 16, right: 16, bottom: 10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(color: Colors.black12, offset: Offset(0, 10), blurRadius: 10)
                        ]
                    ),
                    height: 100,
                    child: Row(
                      children: <Widget>[
                        CircularImage(bloc.list[index].autor.photo, vertical: 6.0, horizontal: 6.0),
                        Expanded(
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(left: 5),
                                    child: Text(
                                        '@${bloc.list[index].autor.username}',
                                        style: TextStyle(color: Colors.black)
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 4),
                              Row(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(left: 5),
                                    child: Text(
                                        bloc.list[index].message,
                                        maxLines: 2,
                                        style: TextStyle(fontSize: 15, height: 1.5, color: Colors.black, fontFamily: "Roboto")
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(height: 4),
                              Container(
                                margin: EdgeInsets.only(right: 20),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    GestureDetector(
                                     onTap: () {
                                       Navigator.pushNamed(context,
                                           ItemMessagesPage.routeName,
                                           arguments:  MessageArguments(index)
                                       );
                                     },
                                     child: Icon(
                                        Icons.message,
                                        color: Colors.black26,
                                      )
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        
                                      },
                                      child: Icon(
                                        Icons.alternate_email,
                                        color: Colors.black26,
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {

                                      },
                                      child: Icon(
                                        Icons.share,
                                        color: Colors.black26,
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                )
              )
          )
        ],
      ),
    );
  }
}
